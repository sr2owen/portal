Automation Plugins
==================

Base
----

.. autoclass:: app.terraform.BaseAutomation
   :members:
   :undoc-members:

Terraform
---------

.. autoclass:: app.terraform.terraform.TerraformAutomation
   :members:
   :undoc-members:
