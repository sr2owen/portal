from typing import Any

import requests

from app.terraform.block_mirror import BlockMirrorAutomation


class BlockRoskomsvobodaAutomation(BlockMirrorAutomation):
    """
    Automation task to import Russian blocklist from RosKomSvoboda.

    This task will import the Russian state register of prohibited sites,
    which is part of the enforcement of federal laws of the Russian Federation
    No. 139-FZ, No. 187-FZ, No. 398-FZ and a number of others that regulate
    the dissemination of information on the Internet.

    Where proxies are found to be blocked they will be rotated.
    """
    short_name = "block_roskomsvoboda"
    description = "Import Russian blocklist from RosKomSvoboda"
    frequency = 300

    _data: Any

    def fetch(self) -> None:
        self._data = requests.get("https://reestr.rublacklist.net/api/v3/domains/").json()

    def parse(self) -> None:
        self.patterns.extend(["https://" + pattern for pattern in self._data])
