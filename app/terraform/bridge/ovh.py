from app.terraform.bridge import BridgeAutomation


class BridgeOvhAutomation(BridgeAutomation):
    short_name = "bridge_ovh"
    description = "Deploy Tor bridges on OVH Public Cloud"
    provider = "ovh"

    template_parameters = [
        "ovh_cloud_application_key",
        "ovh_cloud_application_secret",
        "ovh_cloud_consumer_key",
        "ovh_openstack_user",
        "ovh_openstack_password",
        "ovh_openstack_tenant_id",
        "ssh_public_key_path",
        "ssh_private_key_path"
    ]

    template = """
    terraform {
      {{ backend_config }}
      required_providers {
        random = {
          source = "hashicorp/random"
          version = "3.1.0"
        }
        openstack = {
          source  = "terraform-provider-openstack/openstack"
          version = "~> 1.42.0"
        }
        ovh = {
          source  = "ovh/ovh"
          version = ">= 0.13.0"
        }
      }
    }

    provider "openstack" {
      auth_url    = "https://auth.cloud.ovh.net/v3/"
      domain_name = "Default" # Domain name - Always at 'default' for OVHcloud
      user_name = "{{ ovh_openstack_user }}"
      password = "{{ ovh_openstack_password }}"
      tenant_id = "{{ ovh_openstack_tenant_id }}"
    }

    provider "ovh" {
      endpoint           = "ovh-eu"
      application_key    = "{{ ovh_cloud_application_key }}"
      application_secret = "{{ ovh_cloud_application_secret }}"
      consumer_key       = "{{ ovh_cloud_consumer_key }}"
    }

    locals {
      public_ssh_key = file("{{ ssh_public_key_path }}")
      private_ssh_key = file("{{ ssh_private_key_path }}")
    }

    data "ovh_cloud_project_regions" "regions" {
      service_name = "{{ ovh_openstack_tenant_id }}"
      has_services_up = ["instance"]
    }

    {% for group in groups %}
    module "label_{{ group.id }}" {
      source  = "cloudposse/label/null"
      version = "0.25.0"
      namespace = "{{ global_namespace }}"
      tenant = "{{ group.group_name }}"
      label_order = ["namespace", "tenant", "name", "attributes"]
    }
    {% endfor %}

    {% for bridgeconf in bridgeconfs %}
    {% for bridge in bridgeconf.bridges %}
    {% if not bridge.destroyed %}
    resource "random_shuffle" "region_{{ bridge.id }}" {
      input = data.ovh_cloud_project_regions.regions.names
      result_count = 1

      lifecycle {
        ignore_changes = [input] # don't replace all the bridges if a new region appears
      }
    }

    module "bridge_{{ bridge.id }}" {
      source = "{{ terraform_modules_path }}/terraform-openstack-tor-bridge"
      region = one(random_shuffle.region_{{ bridge.id }}.result)
      context = module.label_{{ bridgeconf.group.id }}.context
      name = "br"
      attributes = ["{{ bridge.id }}"]
      ssh_key = local.public_ssh_key
      ssh_private_key = local.private_ssh_key
      contact_info = "hi"
      distribution_method = "{{ bridge.conf.method }}"
    }

    output "bridge_hashed_fingerprint_{{ bridge.id }}" {
      value = module.bridge_{{ bridge.id }}.hashed_fingerprint
    }

    output "bridge_bridgeline_{{ bridge.id }}" {
      value = module.bridge_{{ bridge.id }}.bridgeline
      sensitive = true
    }
    {% endif %}
    {% endfor %}
    {% endfor %}
    """
