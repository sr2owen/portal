#  pylint: disable=too-few-public-methods

import builtins
from typing import Dict, List, Union

from flask import current_app
from pydantic import BaseModel, Field
from pydantic.typing import Optional
from tldextract import extract

from app.models.base import Group, Pool
from app.models.mirrors import Proxy


class MMMirror(BaseModel):
    origin_domain: str = Field(description="The full origin domain name")
    origin_domain_normalized: str = Field(description="The origin_domain with \"www.\" removed, if present")
    origin_domain_root: str = Field(description="The registered domain name of the origin, excluding subdomains")
    valid_from: str = Field(description="The date on which the mirror was added to the system")
    valid_to: Optional[str] = Field(description="The date on which the mirror was decommissioned")


class MirrorMapping(BaseModel):
    version: str = Field(
        description="Version number of the mirror mapping schema in use"
    )
    mappings: Dict[str, MMMirror] = Field(
        description="The domain name for the mirror"
    )
    s3_buckets: List[str] = Field(
        description="The names of all S3 buckets used for CloudFront logs"
    )

    class Config:
        title = "Mirror Mapping Version 1.1"


def mirror_mapping(_: Optional[Pool]) -> Dict[str, Union[str, Dict[str, str]]]:
    return MirrorMapping(
        version="1.1",
        mappings={
            d.url.lstrip("https://"): MMMirror(
                origin_domain=d.origin.domain_name,
                origin_domain_normalized=d.origin.domain_name.replace("www.", ""),
                origin_domain_root=extract(d.origin.domain_name).registered_domain,
                valid_from=d.added.isoformat(),
                valid_to=d.destroyed.isoformat() if d.destroyed is not None else None
            ) for d in Proxy.query.all() if d.url is not None
        },
        s3_buckets=[
            f"{current_app.config['GLOBAL_NAMESPACE']}-{g.group_name.lower()}-logs-cloudfront"
            for g in Group.query.filter(Group.destroyed.is_(None)).all()
        ]
    ).dict()


if getattr(builtins, "__sphinx_build__", False):
    schema = MirrorMapping.schema_json()
